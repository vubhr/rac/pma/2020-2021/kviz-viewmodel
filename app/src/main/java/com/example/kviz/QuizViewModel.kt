package com.example.kviz

import android.util.Log
import androidx.lifecycle.ViewModel

private const val TAG = "QuizViewModel"
class QuizViewModel : ViewModel() {

    init{
        Log.d(TAG,"View model instance created")
    }

    override fun onCleared() {
        super.onCleared()
        Log.d(TAG, "View model instance is about to be destroyed!")

    }

    var currentIndex = 0
    private val questionList = listOf(
        Question(R.string.question_subject, false),
        Question(R.string.question_android, false),
        Question(R.string.question_cro, true))

    val currentQuestionAnswer: Boolean
        get() = questionList[currentIndex].answer

    val currentQuestionText: Int
        get() = questionList[currentIndex].textResId

    fun moveToNext() {
        currentIndex = (currentIndex + 1) % questionList.size
    }

}