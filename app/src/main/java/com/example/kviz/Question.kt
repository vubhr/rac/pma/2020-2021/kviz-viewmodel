package com.example.kviz

data class Question(var textResId:Int, var answer: Boolean)
